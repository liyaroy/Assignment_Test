class AddStudentIdToMark < ActiveRecord::Migration[5.0]
  def change
    add_column :marks, :student_id, :integer
    add_column :marks, :subject_id, :integer
  end
end
